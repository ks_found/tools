
Gospeller, a name given (1) to the followers of Wycliffe as distributers of the Bible; (2) Evangelists; (3) The reader of the gospel at the altar during communion service; (4) Of those in England during the sixteenth century who professed to be great readers of the Bible and went about preaching.
