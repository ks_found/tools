
Pastoral Staff, or Crozier, a bishop’s official emblem. It is a long staff with a hook at the end, like a shepherd’s crook, and is the symbol of the bishop’s pastoral authority over his flock. It is often beautifully decorated with gold and jewels.
