
Jeter, Jeremiah B., D. D., a Baptist minister of great influence; b. in Bedford County, Va., July 18, 1802; d. in Richmond, Feb. 25, 1880. He entered the ministry in 1822, and spent most of his life in Richmond.
