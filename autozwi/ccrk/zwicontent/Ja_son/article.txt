
Ja'son, (1) the name of several Jews who were prominent during the period of the Maccabees. (2) A Thessalonian Christian with whom Paul lived at Thessalonica. (Acts xvii. 5-9.) He was probably a relation of Paul. (Rom. xvi. 21.)
