
Haygood, Atticus Greene, D. D. (Emory College, Oxford, Ga., 1870), LL. D. (Southwestern University, Georgetown, Tex., 1884), Methodist Episcopal Church

South; b. at Watkinsville, Ga., Nov. ig, 1839; was graduated at Emory College, Oxford, Ga., 1859; entered the ministry, and from 1870 to 1875 was Sunday-school secretary of the M. E. Church South; president of Emory College, 1876-1884; agent of the “John F. Slater Fund,” 1885-1890; in 1890 elected bishop. He is the author of: Our Children (1876); Our Brother in Black (1881); Sermons and Speeches (1883).
