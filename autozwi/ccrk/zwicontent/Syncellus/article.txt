
Syncellus, a term having several significations. (1) At first it was a name given to any monk who shared a cell with another. (2) The attendant of a bishop or abbot. (3) An ecclesiastical dignitary. The highest dignitaries in the Greek Church are called Syncelli.
