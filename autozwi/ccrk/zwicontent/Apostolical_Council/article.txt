
Apostolical Council, a title sometimes given to the assembly of the apostles, of which an account is given in the fifteenth chapter of the Acts. It is also sometimes called “The Council of Jerusalem.”
