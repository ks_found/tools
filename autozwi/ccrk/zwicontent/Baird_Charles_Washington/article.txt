
Baird, Charles Washington, D. D., Presbyterian; b. at Princeton, N. J., Aug. 28, 1828; was graduated at the University of the City of New York, 1848, and at Union Theological Seminary, 1852; became pastor at Rye, N. Y., in 1861, where he remained until his death, Feb. 10, 1887. His best-known work is a History of the Huguenot Emigration to America (New York, 1885), 2 vols.
