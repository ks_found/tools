
Anato'lius, bishop of Constantinople; d. 458. He is the author of several hymns, the best known of which is the one beginning, “Fierce was the wild billow,” translated by John Mason Neale. Hymns of the Eastern Church (London, 1863).
