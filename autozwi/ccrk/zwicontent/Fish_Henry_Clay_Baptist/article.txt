
Fish, Henry Clay, Baptist; b. at Halifax, Vt., Jan. 27, 1820; d. in Newark, N. J., Oct. 2, 1877. A graduate of Union Theological Seminary, New York City, 1845, he was for five years pastor at Somerville, N. J., and from 1850 until his death, pastor of the First Baptist Church of Newark. He was an able and success-

€ 340; ful minister, and in the midst of earnest pastoral labors prepared several volumes: Primitive Piety Revived (18 5 5) [2 0, 000 copies sold in two years]; History and Repository of Pulpit Rloqlienee (1856); Pulpit Eloquence of tlie Nineteenth Century (iSsif, Handbook of Revivals (1874); Eible Lands (18/6).
