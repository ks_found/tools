
Mass-Priests were, in early times, secular priests, as distinguished from regular: afterward they were priests who, by special appointment, said masses in chapels or at particular altars for the souls of the wealthy donors who had endowed them.
