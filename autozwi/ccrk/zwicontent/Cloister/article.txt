
Cloister {clanstrum, an enclosure). The word originally was applied strictly to the wall or enclosure of a monastery. It is now more commonly used to designate the quadrangle of a monastery, one side of which is generally formed by the church, and the others by the conventual buildings, and which frequently has an arcade or colonnade running round the sides.
