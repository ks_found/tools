
Gratian, a monk of the Camaldolensian order, and famous as the author of that corpus decretorum or decretum which bears his name. He lived in the middle of the twelfth century. See Canon Law.
