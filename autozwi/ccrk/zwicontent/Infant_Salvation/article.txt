
Infant Salvation. Whatever views may have been affirmed by theologians in the past, of the Lutheran and Calvinistic faith, it is now universally held that all infants who die in infancy are saved. Dr. Charles Hodge asserts that this is the “common doctrine of evangelical Protestants” (Yvj-tematic Theology, i. 26).
