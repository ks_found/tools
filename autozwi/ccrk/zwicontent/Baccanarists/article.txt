
Baccanarists. At the time the Jesuits were temporarily suppressed in 1773, Nicolas Baccanari attempted to revive the order under the title of Clerks of the Faith of Jesus. The Baccanarists never prospered, though favored by Pope Pius VI., and when the Jesuits were reestablished in 1814, they were absorbed into them. See Jesuits.
