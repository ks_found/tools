
Clearstory (or clerestory), the upper part of the centre aisle of a church, raised above the roofs of the adjoining side-aisles, with windows to light the nave below.
