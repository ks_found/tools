
Paul IV. (John Caraffa), one of the most determined enemies pf the Reformation, succeeded to the popedom in 1555. He had previously been instrumental in establishing the Inquisition in Rome, with a view of stopping the progress of the Reformation in Italy. He was a man of strict life and of determined will, and left his mark upon the whole future history of the papacy.
