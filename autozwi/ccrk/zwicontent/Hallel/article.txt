
Hallel {praise'}. Psalms cxiii.-cxviii. were so called because each of them begins with Hallelujah. They were sung in the temple on the first of the month, and at the feasts of Dedication, Tabernacles, Weeks, and the Passover. The “hymn” sung by our Lord and his disciples at the close of the Last Supper was the second part of the Hallel (Psa. cxv.-cxviii.).
