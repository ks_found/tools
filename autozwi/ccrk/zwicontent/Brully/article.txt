
Brully (broo-lej'), Peter, the successor of Calvin in Strasburg, and a martyr to the Protestant faith; b. near Metz, Germany, about 1518; burned at Tournay, Feb. 19, 1545.
