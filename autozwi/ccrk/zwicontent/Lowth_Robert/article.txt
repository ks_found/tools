
Lowth, Robert, b. at Winchester, Nov. 27, 1710; d. at Fulham, Nov. 3, 1787. Educated at Winchester and Oxford, he became archdeacon of Winchester, 1750, and successively bishop of St. David’s (1766), of Oxford (1766), and of London (1777). The works upon which his fame rests are his lectures on the Poetry of the Hebrews and the translation of the Prophet Isaiah.
