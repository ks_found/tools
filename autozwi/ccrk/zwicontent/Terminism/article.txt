
Terminism, a doctrine which occasioned a controversy at Leipzig in the seventeenth century, the chief movers in which were Reichenberg, who upheld the doctrine, and Ittig, who denied it.. It is the belief that there is a terminus in each man’s life, which only occurs once, after which he is no longer capable of receiving grace or pardon for his sins.
