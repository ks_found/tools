
Vincent, St., a native of Saragossa, and one of the most celebrated martyrs of the ancient Church. He was archdeacon of the church of Saragossa, and suffered martyrdom during the persecution of Diocletian, about 303.
