
Polity. See Church Government
Pollock, Robert, a Scottish minister and poet; b. at Muirhouse, Eaglesham Parish, Renfrewshire, 1799; d. at Southampton, Sept. 15, 1827. He was graduated at the University of Glasgow, and studied theology, but after receiving his license from the United Secession Church (1827), he preached but once. He wrote numerous stories which were published anonymously, but his fame rests upon his poem, the “Course of Time”(1827), which had great popularity.
