
Three-Chapter Controversy, The, grew out of the Monophysite Controversy. Through the influence of Theodorus Asci-das, bishop of Caesarea in Cappadocia, the Emperor Justinian was led to believe that many of the Monophysites might be won to the Church if the chief representatives of the Nestorian theology were rebuked. He therefore issued an edict in 544, condemning (1) the person and writings of Theodore of Mopsuestia; (2) the writings of Theodoret in defence of Nestorius, and (3) the letter
Thummim. See Urim and Thummim.
