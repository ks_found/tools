
Hildegarde, St., b. 1098; d. 1178. She founded the monastery of Rupertsberg. She received prophetical visions, which were approved by the pope, and her influence, especially in the German Church, was very great. She wrote several treatises, and a collection of her letters was published at Cologne in 1566.
