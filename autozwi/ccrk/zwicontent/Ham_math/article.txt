
Ham'math (hot springs'), a fortified city in Naphtali. (Josh. xix. 35.) It is probably identical with Hammam or “warm springs,” about one mile south of Tiberias. Its waters are hot and sulphurous, and too nauseous to drink, but are considered very efficacious for medicinal purposes. The walls of an old town have been found in the vicinity of the baths.
