
Mandyas, an ecclesiastical vestment worn by Greek monks, and sometimes by bishops. It resembles the cope, and reaches almost to the feet.
