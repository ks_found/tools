
Han'nah, wife of Elkanah of Ramathaim-Zophim. (1 Sam. i. I, 2.) In answer to prayer she was given a son, Samuel. Her wonderful song of praise at his birth is given in 1 Sam. ii. 1-10. The name Hannah is a favorite among the Hebrews and Phoenicians.
