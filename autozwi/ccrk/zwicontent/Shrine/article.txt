
Shrine, a repository for relics, either movable or in a tomb. It was customary for pilgrims to come long distances to visit the shrines of eminent saints, and make valuable offerings. Movable shrines were carried in processions around which lamps were kept burning.
