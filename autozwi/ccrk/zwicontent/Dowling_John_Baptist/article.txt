
Dowling, John, Baptist; b. in Sussex, Eng., May 12, 1807: d. at Middletown, N. Y., July 4, 1878. For many years he was pastor of the Berean Baptist Church in New York City. He is best known by his History of Romanism (1845, new ed. 1871).
