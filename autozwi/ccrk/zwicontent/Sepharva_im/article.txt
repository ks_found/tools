
Sepharva'im, a city of Northern Babylonia. (2 Kings xvii. 24, 31; xviii. 34; xix. 13; Isa. xxxvi. 19; xxxvii. 13.) Rawlinson and others identify it with Sippara, a town on the Euphrates between flit and Babylon. The site of the city was discovered in 1881, by Hormuzd Rassam, who unearthed the ruins of its famous sun-temple.
