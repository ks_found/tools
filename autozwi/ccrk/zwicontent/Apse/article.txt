
Apse, the semicircular or polygonal termination to a church. This form was probably derived from the concha or bema, in the classic and early Christian basilica.
