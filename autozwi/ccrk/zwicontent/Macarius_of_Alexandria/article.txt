
Macarius of Alexandria, a famous hermit-priest of the fifth century. Five thousand monks are said to have been trained by him in the Nitrian desert. He is the reputed author of the Bules of the Monks, in thirty chapters, and suffered severe persecutions from the Arians.
