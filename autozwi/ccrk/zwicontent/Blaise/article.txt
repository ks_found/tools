
Blaise, St., Bishop of Sebaste, in Cappadocia; beheaded in the Diocletian persecution, after suffering torture by having his flesh torn with the iron combs used by wool-combers (316). He is the patron saint of wool-combers, and his name and day are still popular in parts of England, where woolen manufactures are carried on.
