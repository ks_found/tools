
Chora'zin, a city of Galilee, associated with Capernaum and Bethsaida in the woes pronounced by Christ. (Matt. xi. 21; Luke x. 13.) Dr. W. M. Thomson identifies it with Kherazeh, two miles west of Tell
Hum, where there are extensive ruins.
