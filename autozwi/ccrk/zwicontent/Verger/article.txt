
Verger, supposed to be derived from the Latin word virga, a twig. The name is given to the officers of the cathedral who carry the mace, or verge, before the clerical dignitaries.
