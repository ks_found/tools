
Censer, a portable metal vessel used for receiving from the altar burning coals, on which the priest sprinkled the incense for burning. (2 Chron. xxvi. 16, 18, 19; Luke i. 9.)
