
She'ol (Heb. equivalent for the Greek Hades'), a word denoting the under-world, the place of shades. It is derived from a word meaning “to penetrate,” “to go down deep;” hence sheol is literally that which is sunk deep. See Hades.
