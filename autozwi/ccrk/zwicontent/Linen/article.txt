
Linen. The Egyptian linen, owing to the -excellent quality of the flax, was equal to the best now made, in texture, and superior in evenness. It was this material from which state robes were made in which mummies were wrapped. The veil of the temple and the curtain for its entrance were made of fine linen (Exod. xxvi. 31, 36), and it was worn by priests and royal personages. (Ex. xxviii. 6, 8, 15, 39; xxxix. 27; 1 Chron. xv. 27.)
