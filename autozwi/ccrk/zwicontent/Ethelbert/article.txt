
Ethelbert, king of Kent (560-616); d. 616. He married Bertha, a daughter of the king of Paris. She was allowed to practice her religion as a Christian princess, but it was not until the coming of Augustine in 5
