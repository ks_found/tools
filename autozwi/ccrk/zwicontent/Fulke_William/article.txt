
Fulke, William, D. D., a famous Puritan divine; b. in London, 1538; d. at Den-nington, Suffolk, Aug. 28, 1589. He was educated at Cambridge, and in 1578 was elected Master of Pembroke Hall and Margaret professor of divinity. “In force of argument he was one of the ablest divines of his time, and one of the principal opponents of the Roman Church.” He was a prolific writer.
