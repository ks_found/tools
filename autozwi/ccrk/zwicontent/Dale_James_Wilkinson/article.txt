
Dale, James Wilkinson, D. D., Presbyterian; b. in New Castle Co., Del., Oct. 16, 1812; d. in Media, Penn., April 19, 1881. A graduate of the University of Pennsylvania (1831), and Andover Theological Seminary, he was agent of the Bible
Society of Philadelphia for seven years, and pastor, from 1845 to 1876, of Presbyterian churches at Ridley, Media, and Wayne. His reputation rests upon the elaborate works which he published in defence of pedobaptism: Classic Baptism (Phila., 1867) ; Judaic (1871) ; Johannic (1872) ; Christie and Patristic (1874).
