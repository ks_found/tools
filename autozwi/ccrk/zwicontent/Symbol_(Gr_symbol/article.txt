
Symbol (Gr. symbolon, that which is thrown together with) denotes a sign or emblem. Originally it had reference to the Apostles’ Creed as a confession or sign distinguishing Christians from all others. Luther and Melanchthon first applied the words to Protestant creeds. Baptism and the Lord’s Supper are now spoken of as symbols, or visible signs, of an invisible salvation.
