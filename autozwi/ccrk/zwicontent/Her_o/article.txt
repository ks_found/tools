
Her'od, “the name of a family which rose to power in Judea during the period which immediately preceded the complete destruction of the Jewish nationality. The family was of Idumean descent; but, though alien in blood, was Jewish in religion, the Idumeans having been conquered and converted to Judaism by John Hyr-canus, 130 B. c. The most remarkable rulers of the name are four in number— Herod the Great, Herod Antipas, and Herod Agrippa I. and II. (for the last two, see Agrippa), (i) Herod the Great. He was the second son of Antipater, who was appointed procurator of Judea by Julius Caesar, 47 B. C. At the time of his father's elevation, Herod, though only fifteen years of age, was made governor of Galilee, and afterward of Coele-Syria; and finally he and his elder brother were made joint-tetrarchs of Judea; but he was soon displaced by Antigonus, the representative of the Asmonean dynasty, and forced to flee to Rome, where he obtained, through the patronage of Antony, a full recognition of his claims, together with the title of King of Judea, 40 B. C. Several years elapsed, however, before he succeeded in establishing himself in Jerusalem. On the fall of Antony, he managed to secure a continuance of favor from Augustus, from whom he not only obtained a confirmation of his title to the kingdom, but also a considerable accession of territory, 31 B. c. From this time till his death his reign was undisturbed by foreign war; but it was stained with cruelties and atrocities of a character almost without parallel in history. Every member of the Asmonean family, and even those of his own blood, fell in succession, a sacrifice to his jealous fears; and in the latter years of his life, the lightest shade of suspicion sufficed as the ground for his wholesale butcheries, which are related in detail by Josephus. Of these, the one with which we are best acquainted is the slaughter of the infants at Bethlehem. The one eminent quality by which Herod was distinguished was his love of magnificence in architecture, and the grandeur of the public works executed under his direction.
Even by these, however, he alienated the Jews, who ascribed them all to his Gentile leanings, and to a covert design of subverting the national religion. Herod married no fewer than ten wives, by whom he had fourteen children. He died of a loathsome disease at the age of seventy, after a reign of thirty-seven years. (2) Herod Antipas, son of Herod the Great by his wife Malthace, a Samaritan, was originally designed by his father as his successor; but, by the final arrangements of the will of Herod the Great, Antipas was named tetrarch of Galilee and Perea. He divorced his first wife, the daughter of Aretas, king of Arabia Petrea, in order to marry Herodias, the wife of his half-brother Philip—

(From Lewin’s Life and Epistles of Saint Paul.')
Antipater, of Idumaea, in. Cypros. d. B. c. 48.
Phasael. HEROD the GREAT. Joseph. (“Herod the king,” Matt. ii. i.)
d. B. C. 4. married.
Pheroras.
Salome. d. a. d. 10.
Doris. MARIAMNE,
I dau. of Alexander |	the Asmonaean.
Antipater.
Pal as.
Phasael. d. b. c. 4.
Phsedra.	Mariamne, dau. of Simon.
Roxana. Philip.
(Matt. xiv. 3.) m. Herodias.
Salome.
(Matt. xiv. 6.)
Malthace. Cleopatra. Elpis d. b. c. 4.
