
Peru. The great majority of the people, who are of Indian descent, are Roman Catholics. The Church is still wealthy, although, since the establishment of the republic, much of the property which it held under Spanish rule has been confiscated. The bishops are appointed by the government and are treated as government officials.
