
Fulgentius, bishop of Ruspe, in Africa; b. at Telepte, in North Africa, in 468; d. at Ruspe, Jan. 1, 533. He was made bishop in 508, and two years later banished from Africa by the Vandal king, Thrasimund (who was an Arian), to Cagliari, Sardinia. In 523 he was allowed to return. He was recognized as one of the strongest opponents of the Arians. See his works in Migne: Fat. Lat. LXV.
