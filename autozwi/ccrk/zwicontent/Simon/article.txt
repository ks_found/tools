
Simon (a hearing), contracted from Sim'eon. (i) A famous sorcerer, who professed to be a convert to the Christian faith. (Acts viii. 9.)
(2)	Simon Peter. (Matt, iv.’ 18.) See Peter.
(3)	Simon the Canaanite (Matt. x. 4), or Simon Zelotes, one of the twelve apostles. He was a member of the party called Zealots, hence his name.
(4)	The brother of our Lord. (Matt. xiii. 55; Mark vi. 3.)
(5)	A Pharisee. (Luke vii. 36.)
(6)	A leper. (Matt. xxvi. 6.)
(7)	The father of Judas Iscariot. (John vi. 7I-)
(8)	The Cyrenian who was compelled to bear our Saviour’s cross, when the latter could no longer carry it. (Matt, xxvii. 32, etc.)
(9)	The tanner with whom Peter lodged at Joppa. (Acts ix. 43.)
