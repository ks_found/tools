
Pat'mos, a barren and rocky island, situated near the coast of Asia Minor, in the Aegean Sea. The Roman emperors used it as a place of banishment, and here the Apostle John wrote his Revelation. (Rev. i. 9.) Above the cave, where tradition says he had his visions, is the Greek monastery built by Alexius Commenus.
