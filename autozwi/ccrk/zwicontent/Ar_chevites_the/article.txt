
Ar'chevites,“the name of a people transplanted by the Assyrians into the depopulated Samaria. (Ezek. iv. 9.) They were inhabitants of Erech and its neighborhood, mentioned in Gen. x. 10 as belonging to the kingdom of Nimrod. Erech has been identified in the ruins of Warka, on the banks of the Euphrates, eighty-two miles south-east from Babylon.”—Wolf Baudis-sin.
