
Passionists, an order of the Roman Catholic Church founded by Paolo della Croce; b. at Ovada, in Piedmont, Jan. 3, 1694; d. at Rome, Oct. 18, 1775. He was canonized’by Pius IX. in 1868. The object of the order is to keepalive in every possible way Christ’s atoning passion and death. There are several congregations of this order in England and the United States.
