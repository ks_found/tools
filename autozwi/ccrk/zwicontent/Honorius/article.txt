
Honorius is the name of four popes and an antipope. See Popes. Honorius I. (625-683) sided, in the Monothelitic controversy, with the Monothelites and was anathematized by the sixth oecumenical council (680). The fact that a pope had been a heretic was the cause of great discussion in connection with the adoption of the doctrine of papal infallibility.
