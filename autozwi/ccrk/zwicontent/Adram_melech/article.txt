
Adram'melech (honor of the king), (1) an idol of the Sepharvites whom Shalmaneser brought to Samaria after carrying their inhabitants captive to Assyria. (2 Kings xvii. 31.) (2) A son of Sennacherib, king of Assyria, who, with his brother Sharezer, slew their father in the temple of Nisroch, B.C. 721. (2 Kings xix. 37.)
