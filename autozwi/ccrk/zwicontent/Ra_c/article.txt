
Ra'ca, a term of contempt often used by the Jews. (Matt. v. 22.) It is derived from the Aramaic reha, “worthless.” It is a less severe term of opprobrium than “fool.”
Ra'chel. See Jacob.
