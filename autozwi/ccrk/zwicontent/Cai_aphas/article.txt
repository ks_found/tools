
Cai'aphas (depression), high-priest of the Jews under Tiberius during the years of j our Lord’s public ministry, and at the time

(>36)

of his trial and crucifixion. In character he was coarse and brutal, but adroit and crafty. (Matt. xxvi. 3, 4; John xi. 49, 50; xviii. 14.) He it was who instigated the murder of Jesus, and afterward persecuted his followers. (Acts iv. 6, 17.) He was deposed by Vitellius, A. D. 36. he was protected. He went into the land of Nod, on the east of Eden, where he built a city, which he named after his son, Enoch. His descendants are enumerated, with the inventions for which they were remarkable.
