
Booth, William, General of the Salvation Army; b. at Nottingham, England, April 10, 1829. He was first a minister in the Methodist New Connection, but in 1865 resigned, and devoted himself entirely to evangelistic labors. From “the Christian Mission,” started in the East End of London, he organized the “Salvation Army,” now so widely known. See Salvation Army.
