
Reredos (French arriere dos), the screen at the back of an altar. Beautiful examples are to be seen in some of the English cathedrals.
