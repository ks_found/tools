
Pantheon, once a heathen temple at Rome, dedicated to Jupiter and all the gods. It was re-dedicated by Pope Boniface IV., in 608, to the Virgin Mary and the saints.
