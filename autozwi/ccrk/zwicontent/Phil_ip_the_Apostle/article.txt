
Phil'ip the Apostle. His name in the Synoptical Gospels and  the Acts occurs only in the list of apostles. (Matt. x. 3; Mark iii. 18; Luke vi. 14; Acts i. 13.) He is frequently mentioned in the Gospel by John. According to tradition he preached in Phrygia, and died at Hierapolis.
