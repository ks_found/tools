
Jehoi'ada (70horn Jehovah knows), a high-priest of the Jews, and the husband of Jehosheba. He was the guardian of Joash, and having killed Athaliah he wisely directed the affairs of the young king during his life-time. In honor of his eminent services he was buried “in the city of David, among the kings.” (2 Chron. xxiv. 16.)
