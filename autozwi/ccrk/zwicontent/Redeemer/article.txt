
Redeemer, Orders of the. (i) The order in Spain was founded by Alfonso I., as a reward for courage in fighting against the Moors. It was abolished after their conquest. (2) In Italy, by Vincenzo of Mantua, for the defence of the Catholic faith: abolished in the eighteenth century. (3) In Greece, by King Otto I., in 1844, as a reward of merit. The king is the Grand Master.
