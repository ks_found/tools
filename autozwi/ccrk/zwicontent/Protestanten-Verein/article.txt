
Protestanten-Verein (Protestant Union), an association of German rationalistic ministers and professors, organized in 1865 at Eisenach. It has been earnestly opposed by the orthodox influence in the German Church, and has made but little progress.
