
Shastra, or Shastras, a name applied to the sacred books of the Hindus. They contain their law, both civil and religious, and are said to have been collected by Manu, the son of Brahma.
