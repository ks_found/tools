
Faith, Rule of. See Regula Fidel
Fakir (Arabic, floor wan), the name given a class of Hindoo mendicants. They have existed in India from a very early period, and now number some two millions. They seek to excite pity by self-inflicted tortures, and, in times past, have proved a dangerous element in society by their mad fanaticism. The English government has put a stop to some of their worst practices, but they are still dreaded, if not respected, by the people. They seek the reoutation of “saints,” but it is doubtful if there is any religious sentiment in their action.
