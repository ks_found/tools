
Berni'ce, or Bereni'ce (victorious), the eldest daughter of Herod Agrippa I. (Acts xxv. 13, 23; xxvi. 30.) She was first married to her uncle Herod, the king of Chal-cis; and after his death lived in a connection with her brother Agrippa that gave rise to much scandal. She finally became the mistress of Vespasian, and then of his son Titus.
