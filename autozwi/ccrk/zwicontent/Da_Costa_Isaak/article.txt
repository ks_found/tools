
Da Costa, Isaak, b. in Amsterdam, 1798; d. there, i860. He belonged to a wealthy and influential family of Portuguese Jews, but embraced the Christian faith in 1821. He was a brilliant poet and did a noble work as a Christian apologist, especially in opposing the Tubingen school. The Four Witnesses, an important work from his pen, was translated into English by D. Scott (London, 1851).
