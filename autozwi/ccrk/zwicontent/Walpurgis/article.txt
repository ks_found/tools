
Walpurgis, or Walpurga, St., a native of England, who spent her life in Germany assisting her brother, St. Willibald, and her uncle, St. Boniface, in their missionary labors. She became abbess of a convent at Heidenheim in Franconia, and died about 777. Many traditions are linked with her name.
