
Ha'dad-Rimmon, or Ha'dar-Rimmon, the name given to a locality which witnessed the death of Josiah (2 Kings xxiii. 29), whose memory was honored by songs of lamentation. The location was probably at the site of the modern Rummane, in the plain of Jezreel. The name of the town, Hadad-Rimmon, was, no doubt, originally the name of a deity, Hadad and Rimmon being both the names of gods.
