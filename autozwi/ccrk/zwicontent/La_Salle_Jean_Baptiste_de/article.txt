
La Salle, Jean Baptiste de, founder of the Ignorantines (see art.); b. at Rheims, 1651; d. at Rouen, 1719. He opened his free schools for the young in 1681,, and met with such success that it led to the founding of the order with which his name is connected. He was canonized by Pius IX. in 1852.
