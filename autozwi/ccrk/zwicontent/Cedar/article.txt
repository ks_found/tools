
Cedar. This name is applied in Scripture to several cone-bearing trees, and once to the juniper-tree of Sinai (Lev. xiv. 4), but ordinarily it refers to the cedar of Lebanon. Several groves of cedars are still found on the mountains of Lebanon. It is distinguished by its gnarled strength, and massive, wide-spreading branches. The wood is hard and fragrant, and takes a high polish.
