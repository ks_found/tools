
Thurible, Thuribulum, a vessel in which incense is burned. It is usually made of gold or silver, with perforations in its cover through which the fumes escape. The censer is suspended by three long chains by which it is swung backward and forward.
