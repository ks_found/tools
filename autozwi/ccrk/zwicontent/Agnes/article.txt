
Agnes, St., is commemorated in the Roman Church, Jan. 21 and 28. She was a Christian virgin, martyred by order of Diocletian. Her chastity, according to tradition, was preserved under the severest trials. She is represented in mediaeval art as followed by a lamb. The women of Rome pray at her shrine for the gifts of meekness and chastity.
