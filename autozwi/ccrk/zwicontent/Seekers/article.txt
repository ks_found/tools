
Seekers, a small Puritan sect which arose in 1645. They professed to be seeking a true church, ministry and sacraments, and according to Baxter were a heterogeneous company comprised of Roman Catholics and infidels, as well as Puritans.
