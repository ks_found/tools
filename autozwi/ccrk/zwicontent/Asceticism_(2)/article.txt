
Asceticism, in its more extreme forms, can hardly be said to enter the practice of Christians who live outside monastic communities; and in modern times such ideas of self-discipline by means of bodily mortification have been superseded, to a large extent, by the idea of duty done in the world, and in the work of life to which Divine Providence has called us.—Benham: Dictionary of Religion.
