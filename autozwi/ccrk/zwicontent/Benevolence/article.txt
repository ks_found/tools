
Benevolence, Beneficence. The first has reference to the desire of the heart to do good to others, and the latter to practical efforts in their behalf. The one is universal in its sympathy, the other is guided in its activities by various circumstances. The rule of life should be to do good to all men as we have opportunity. (Gal. vi. 10.)
