
Michaelmas. This festival, held on Sept. 29, is celebrated not only in the Roman Catholic Church, but also in the Greek and some other Protestant churches, in honor of the Archangel Michael. The festival is a special commemoration of the benefits received from the ministry of angels. There is a tradition that the festival was instituted by Alexander, bishop of Alexandria.
