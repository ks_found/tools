
Kimchi, David, a learned Jewish rabbi; b. in 1160; d. about 1240. He was a prolific writer, and prepared a Hebrew grammar, which has been used as the basis of nearly all modern works. He wrote a commentary on the Psalms, Genesis, and all the prophetical books. His commentary on Zechariah was translated into English by McCaul (London, 1837).
