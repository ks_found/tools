
Eparchy (the Greek word for province'). In ecclesiastical usage it denotes a province governed by a metropolitan. In the Russian Church a bishop is called an eparch.
