
Canker-worm. This name is given in the Bible to the larva, or caterpillar state of the locust. These larvae consume what has been left by the winged locust. (Neh. iii. 15, 16; Joel i. 4.)
