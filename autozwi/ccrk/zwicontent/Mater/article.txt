
Mater Dolorosa (the mourning mother}, a name given to certain pictures of the Virgin, which represent her alone, without the child, and weeping. See Mrs. Jameson: Legends of the Madonna.
