
Asterius, Bishop of Amasia, in Pontus; d. about 410. His fame rests upon his Homilies, which had a great reputation in the Eastern Church. Eleven of them have been preserved, and the fragments of twenty-two others.
