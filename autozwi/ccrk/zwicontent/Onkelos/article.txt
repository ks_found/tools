
Onkelos, the supposed author of an Aramaic version (Targum) of the Pentateuch, and other books of the Old Testament. He was a pupil of Gamaliel, and, as the Talmud informs us, a fellow-scholar with Paul. His Targum is a faithful translation. It may be found in the Bibles of Bomberg and Buxtorf, and in Walton’s Polyglot.
