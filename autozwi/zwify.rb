require 'bundler/setup'
Bundler.require(:default)

# Globals
$volume_code = 'ccrk'
$html_directory = "#{$volume_code}/html"
unless Dir.exist?($html_directory)
  puts "No HTML directory exists at #{$html_directory}"
  exit
end
$volume_zwi_content_directory = "#{$volume_code}/zwicontent" # Define the directory for ZWI files

# Variable globals (will be zeroed out with each ZWI)
$article_zwi_content_directory = ''
$volume_zwi_directory = "#{$volume_code}/zwi"
$metadata = {}
$base_filename = ''

# Ensure the zwicontent and zwi directories exist
FileUtils.mkdir_p($volume_zwi_content_directory) unless Dir.exist?($volume_zwi_content_directory)
FileUtils.mkdir_p($volume_zwi_directory) unless Dir.exist?($volume_zwi_directory)


def get_title(doc)
  # Extract the text content from the <title> tag
  title_text = doc.at_css('title')&.text

  # Store the extracted title text in the metadata hash under the key 'Title'
  $metadata['Title'] = title_text.strip unless title_text.nil?
end

#############################
# MAIN ZWI-CREATION LIBRARY
def prep_article_txt_and_description(doc)
  # PREP DESCRIPTION
  # Assume description starts with the beginning of the <article> text.
  # We no longer need to find a 'desc' anchor.
  description_nodes = doc.css('article p')  # Collect all paragraph nodes within the article.

  # Concatenate the text from the first few paragraphs to form the description.
  description_text = description_nodes.map(&:text).join(" ")[0..350]

  # Further refine the description text.
  description = description_text
    .gsub(" ", " ")    # Replace non-breaking spaces with regular spaces.
    .gsub(/\[\w+\]/, "")    # Strip footnotes or similar markers.
    .gsub(/\s+/, " ")       # Replace multiple whitespaces with one space.
    .gsub(/\n+/, " ")       # Replace newlines with space.
    .strip

  # Truncate the description to end at a complete sentence if possible.
  if description.length > 300
    final_string = description[-41..-1]
    last_punctuation = final_string.rindex(/[.!?;:]\s/)

    if last_punctuation
      to_delete = final_string[last_punctuation+2..-1]
      description = description.chomp(to_delete)
    end
  end

  # Ensure the description exists and is of a reasonable length.
  if description.length > 0
    $metadata['Description'] = description
    # Add to <head>, in <meta name="description"> tag.
    if doc.at_css('meta[@name="description"]')
      doc.at_css('meta[@name="description"]')['content'] = description
    else
      node = doc.create_element('meta')
      node['name'] = "description"
      node['content'] = description
      doc.at_css('head title').before(node)
    end
  else
    puts "*** No description found, hence not saved."
  end

  # WRITE ARTICLE.TXT.
  # Needs much less prep than the description.
  # Note, by this point there should be *only one* article in the text.
  # Regardless, it will print only the first article.
  if doc.css('article')
    article = doc.at_css('article').text
    File.write("#{$article_zwi_content_directory}/article.txt", article)
  else
    puts "Quit at #{$metadata['Title']} when unable to write article.txt."
    exit
  end
end

# In metadata.json, point Content['article.html'] and Content['article.txt']
# to hashes.
def create_article_hashes
  articles = ['article.html', 'article.txt']
  $metadata['Content'] = {}
  articles.each do |article|
    article_text = File.read("#{$article_zwi_content_directory}/#{article}")
    $metadata['Content'][article] = Digest::SHA1.hexdigest(article_text)
  end
end

def write_metadata
  # Script requires that $volume_code always be the same as metadata.json's
  # 'Publisher' field!
  $metadata['Publisher'] = $volume_code
  # Time to add TimeCreated and LastModified.
  $metadata['TimeCreated'] = Time.now.to_i.to_s
  $metadata['LastModified'] = Time.now.to_i.to_s
  # Need to re-sort metadata here.
  format_metadata
  File.write("#{$article_zwi_content_directory}/metadata.json", JSON.pretty_generate($metadata, opts = {space: false, space_before: false}))
  # Remove needless space inside blank arrays.
  mdtextlines = File.read("#{$article_zwi_content_directory}/metadata.json")
  # Actually remove the offending bits.
  mdtextlines = mdtextlines.gsub(/\[\n\n  \]/m, "[]")
  File.write("#{$article_zwi_content_directory}/metadata.json", mdtextlines)
end

# In case new fields were created and appended to bottom, this puts them
# all in the right/expected order.
def format_metadata
  $metadata['GeneratorName'] = 'AutoZWI'
  # All fields, in order.
  fields = %w{ZWIversion GeneratorName Title Lang Content Primary Revisions CollectionTitle Publisher Editors Authors LastModified TimeCreated Categories Topics Ratings Description Comment License SourceURL SourceFileURL LocalFileURL ShortTitle Disambiguator ALternativeTitles InvertedTitle Subarticles Redirect RedirectURL RedirectTitle BookTitle BookTitleLong SeriesTitle BookEdition BookEditors BookVolumeNumber BookPublisher BookCopyrightDate CopyrightBy BookPrintingDate PublicationDate}
  # Iterating in order, reconstruct the data in $metadata in a holder.
  metadata_holder = {}
  fields.each do |field|
    unless $metadata[field].nil?
      metadata_holder[field] = $metadata[field].dup
    end
  end
  # If there are any remaining fields, append them to bottom.
  $metadata.keys.each do |mdkey|
    # Basically, metadata_holder should now have all the fields that
    # $metadata has. But if there are any others in $metadata, copy them
    # to the bottom of metadata_holder.
    next unless metadata_holder[mdkey].nil?
    metadata_holder[mdkey] = $metadata[mdkey].dup
  end
  # Finally, copy the holder into $metadata. No return needed since this
  # manipulates a global.
  $metadata = metadata_holder.dup
end

# Creates zwi/media.json out of current files.
def create_media_json
  # Get array of paths to all data files.
  media_list = Dir.glob("#{$article_zwi_content_directory}/data/**/*.*")
  # Discard $wkdir and 'zwi/' in order to construct media.json keys.
  media_list_no_zwi = media_list.map do |path|
    path.gsub(/.*\/zwi\//, '')
  end
  media_hash = {}
  # Iterate just the paths that will be saved in media.json.
  media_list_no_zwi.each do |path|
    # But you need to reconstruct the full path to get the content.
    content = File.read("#{$article_zwi_content_directory}/#{path}")
    # Construct the media_hash based on the shortened path.
    media_hash[path] = Digest::SHA1.hexdigest(content)
  end
  File.write("#{$article_zwi_content_directory}/media.json", JSON.pretty_generate(media_hash, opts = {space: false, space_before: false}))
end

# Creates signature.json with standard fields and, most importantly, the
# JSON Web Token (JWT). Extensively revised June 2023.
def sign_zwi
  # Values hard-coded for Oldpedia. Should be drawn from DID???
  sig = {"kid": "did:psqr:oldpedia.org#publish",
         "identityName":
           "Oldpedia by the Knowledge Standards Foundation (KSF)",
         "identityAddress":
           "E-mail: info@encyclosphere.org, PO Box 31, Canal Winchester, OH 43110, USA" }
  media_content = File.read("#{$article_zwi_content_directory}/media.json")
  metadata_content = File.read("#{$article_zwi_content_directory}/metadata.json")
  # Create payload of JWT. Also hard-coded.
  timestamp = Time.now
  payload = {
    'iss' => 'Oldpedia by the Knowledge Standards Foundation (KSF)',
    'address' => 'E-mail: info@encyclosphere.org, PO Box 31, Canal Winchester, OH 43110, USA',
    'iat' => timestamp.to_i,
    'metadata' => Digest::SHA1.hexdigest(metadata_content),
    'media' => Digest::SHA1.hexdigest(media_content)
  }
  pk = File.read("identity/publish.private.jwk")
  pk = JSON.parse(pk)
  # Export key.
  key = create_ec_key(pk['x'], pk['y'], pk['d'])
  # Prep header
  header_sig_fields = sig.clone
  header_sig_fields.delete(:identityName)
  header_sig_fields.delete(:identityAddress)
  header = header_sig_fields
             .merge({ typ: 'JWT', jwk: pk.except('d', 'alg', 'kid') })
  # Actually get the signature token.
  token = JWT.encode(payload, key, 'ES384', header)
  sig['token'] = token
  sig['alg'] = 'ES384'
  sig['updated'] = timestamp.gmtime.strftime("%Y-%m-%dT%H:%M:%S.%6N")
  # Write signature.json!
  File.write("#{$article_zwi_content_directory}/signature.json", JSON.pretty_generate(sig))
end

# Grabbed from https://github.com/jwt/ruby-jwt/blob/main/lib/jwt/jwk/ec.rb
# Adapted by Henry Sanger.
def create_ec_key(jwk_x, jwk_y, jwk_d)
  curve = 'secp384r1'
  x_octets = JWT::Base64.url_decode(jwk_x)
  y_octets = JWT::Base64.url_decode(jwk_y)
  point = OpenSSL::PKey::EC::Point.new(
    OpenSSL::PKey::EC::Group.new(curve),
    OpenSSL::BN.new([0x04, x_octets, y_octets].pack(''), 2)
  )
  sequence = if jwk_d
    # https://datatracker.ietf.org/doc/html/rfc5915.html
    # ECPrivateKey ::= SEQUENCE {
    #   version        INTEGER { ecPrivkeyVer1(1) } (ecPrivkeyVer1),
    #   privateKey     OCTET STRING,
    #   parameters [0] ECParameters {{ NamedCurve }} OPTIONAL,
    #   publicKey  [1] BIT STRING OPTIONAL
    # }
    OpenSSL::ASN1::Sequence([
                              OpenSSL::ASN1::Integer(1),
                              OpenSSL::ASN1::OctetString(OpenSSL::BN.new(JWT::Base64.url_decode(jwk_d), 2).to_s(2)),
                              OpenSSL::ASN1::ObjectId(curve, 0, :EXPLICIT),
                              OpenSSL::ASN1::BitString(point.to_octet_string(:uncompressed), 1, :EXPLICIT)
                            ])
  else
    OpenSSL::ASN1::Sequence([
                              OpenSSL::ASN1::Sequence([OpenSSL::ASN1::ObjectId('id-ecPublicKey'), OpenSSL::ASN1::ObjectId(curve)]),
                              OpenSSL::ASN1::BitString(point.to_octet_string(:uncompressed))
                            ])
  end
  OpenSSL::PKey::EC.new(sequence.to_der)
end

class ZipFileGenerator
  # Initialize with the directory to zip and the location of the output archive.
  def initialize(input_dir, output_file)
    @input_dir = input_dir
    @output_file = output_file
  end

  # Zip the input directory.
  def write
    entries = Dir.entries(@input_dir) - %w[. ..]

    ::Zip::File.open(@output_file, create: true) do |zipfile|
      write_entries entries, '', zipfile
    end
  end

  private

  # A helper method to make the recursion work.
  def write_entries(entries, path, zipfile)
    entries.each do |e|
      zipfile_path = path == '' ? e : File.join(path, e)
      disk_file_path = File.join(@input_dir, zipfile_path)

      if File.directory? disk_file_path
        recursively_deflate_directory(disk_file_path, zipfile, zipfile_path)
      else
        put_into_archive(disk_file_path, zipfile, zipfile_path)
      end
    end
  end

  def recursively_deflate_directory(disk_file_path, zipfile, zipfile_path)
    zipfile.mkdir zipfile_path
    subdir = Dir.entries(disk_file_path) - %w[. ..]
    write_entries subdir, zipfile_path, zipfile
  end

  def put_into_archive(disk_file_path, zipfile, zipfile_path)
    zipfile.add(zipfile_path, disk_file_path)
  end
end

# Actually create/update the ZWI file. First, make remaining components.
def write_zwi
  # Create hashes for article.html and article.txt, placed in metadata.json.
  create_article_hashes
  # Write metadata to file; should be ready now.
  write_metadata
  # Create bunch of pointers to hashes of all media files (=media.json).
  create_media_json
  # Actually sign the ZWI file.
  sign_zwi
  # Delete zip file if it exists.
  output_file = "#{$volume_zwi_directory}/#{$base_filename}.zwi"
  File.delete(output_file) if File.exist?(output_file)
  # Make into zip file; code above for this is copied from
  # https://github.com/rubyzip/rubyzip
  ZipFileGenerator.new($article_zwi_content_directory, output_file).write()
end
# end of ZWI library
# ####################


##################
# MAIN EXECUTION

def transform_html_to_zwi(doc)
  # 1. Initialize metadata with title
  $metadata = {}  # Initialize metadata for this article.
  $metadata = JSON.parse(File.read("metadata.json")) # Assumes template.
  get_title(doc)
  $metadata['SourceURL'] = "https://oldpedia.org/article/#{$metadata['Publisher']}/#{$metadata['Title']}"
  # 2. Make ZWI directory for this article
  $article_zwi_content_directory = "#{$volume_zwi_content_directory}/#{$base_filename}"
  FileUtils.mkdir_p($article_zwi_content_directory) unless Dir.exist?($article_zwi_content_directory)
  # 3. Create HTML, TXT, and prep description
  # Copy HTML to the article ZWI file.
  FileUtils.cp("#{$html_directory}/#{$base_filename}.html", "#{$article_zwi_content_directory}/article.html")
  prep_article_txt_and_description(doc)
  # 4. Actually prepare ZWI file.
  write_zwi
end

# Get all HTML files in the directory
html_files = Dir.glob(File.join($html_directory, '*.html'))

# Create a new progress bar
progressbar = ProgressBar.create(
  title: 'Converting HTML to ZWI',
  total: html_files.size,
  format: '%a %B %p%% %t',
  progress_mark: '=',
  remainder_mark: ' '
)


# Iterate over each HTML file in the HTML directory
Dir.glob(File.join($html_directory, '*.html')).each do |html_file_path|
  # Extract the filename without extension to reuse for the ZWI file
  $base_filename = File.basename(html_file_path, '.html')

  # Read the HTML file content
  html_content = File.read(html_file_path)

  # Parse the HTML content with Nokogiri
  doc = Nokogiri::HTML(html_content)

  # Transform the HTML content to ZWI format
  transform_html_to_zwi(doc)

  # Increment the progress bar
  progressbar.increment
end

puts "All #{html_files.size} HTML files have been converted to ZWI format."
