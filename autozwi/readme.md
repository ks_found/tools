# Firstline Finder

## Overview

The Firstline Finder is a Ruby script designed for processing OCR plaintext of old encyclopedias. It identifies the start of each article within the text and applies heuristic-based scoring to distinguish between potential article beginnings and other text. The script aims to format the text for better readability and organization by marking article boundaries.

## Usage

To use the Firstline Finder, you need a plaintext file generated from the OCR of an encyclopedia. The file should be named `encyclopedia.txt` and placed in the same directory as the script.

### Steps to Run:

1. Ensure Ruby is installed on your system.
2. Place your OCR-generated plaintext file, named `encyclopedia.txt`, in the same directory as `firstline_finder.rb`.
3. Open a terminal or command prompt.
4. Navigate to the directory containing `firstline_finder.rb`.
5. Run the script using Ruby:

   ```bash
   ruby firstline_finder.rb
   ```
6. The script will generate several output files, including `encyclopedia-mu.txt`, which contains the processed text with articles marked.

### Editing the Script for Different Encyclopedias:

Currently, to adapt the script to different encyclopedias, you may need to adjust the following within `firstline_finder.rb`:

- **Encyclopedia Filename**: This is the plaintext file (not HTML) outputted from OCR, expected to have reasonably short entries (so, hundreds of items per volume).
- **Volume Code**: If processing a different volume or a different encyclopedia, you may need to change the `volume_code` passed to `process_text` to match specific heuristics or scoring adjustments.
- **Heuristic Adjustments**: Depending on the structure and formatting of the encyclopedia, adjustments to the scoring heuristics in `score_line` and `omit_line?` might be necessary.

## File Types and Assumptions

The script works with plaintext files, specifically formatted from OCR scans of encyclopedias. It assumes the following:

- The text is in English.
- Articles start with capitalized words and are somewhat alphabetically ordered.
- There are identifiable markers or patterns that can differentiate article beginnings from other text.

## Requirements and Goals

The script aims to satisfy several requirements:

- **Article Identification**: Detect the start of articles using a scoring system based on heuristics, such as the presence of a capitalized first letter and punctuation patterns.
- **Scoring System**: Apply a numerical score to each line, considering factors like the preceding line's ending, line length, and alphabetical ordering.
- **Formatting**: Mark the beginning and end of articles for clarity, using `[[article]]` and `[[/article]]` tags.
- **Flexibility**: Accommodate different encyclopedia formats with minimal adjustments.
- **Output Generation**: Create a modified text file (`encyclopedia-mu.txt`) with clear demarcations of articles, alongside auxiliary files (`output.txt`, `low_scorers.txt`, and `high_scorers.txt`) for further analysis and review.

This project leverages Ruby's text processing capabilities to automate the structuring and enhancement of digitized encyclopedia content, facilitating easier access and readability of historical texts.

---

This `README.md` provides a concise guide to understanding, using, and modifying the Firstline Finder script for your specific needs.