# Globals: use to reset the name of the working file.
$input_filename = 'ccrk.txt'
$volume_code = 'ccrk'
$output_filename = $input_filename.gsub('.txt', '-cu.txt')
$output_directory = $input_filename.gsub('.txt', '')
$input_path = "#{$output_directory}/#{$input_filename}"
$output_path = "#{$output_directory}/#{$output_filename}"
unless Dir.exist?($output_directory)
  Dir.mkdir($output_directory)
  puts "Created #{$output_directory}."
end

def prep_array
  File.readlines($input_path).map!(&:rstrip)
end

def fix_selected_punctuation(cleaned_lines)
  print "Fixing selected punctuation issues: "
  # Combine lines into a single string for easier manipulation
  text = cleaned_lines.join("\n")

  # Initialize counters for each correction type
  curly_quotes_count, colon_semicolon_comma_count, comma_space_count, comma_before_year_count, period_space_hyphen_count, single_to_double_quote_count, period_space_correction_count = 0, 0, 0, 0, 0, 0, 0

  # First bit: Remove spaces after opening and before closing curly quotes
  curly_quotes_count += text.scan(/“\s/).count
  text.gsub!(/“\s/, '“')
  curly_quotes_count += text.scan(/\s”/).count
  text.gsub!(/\s”/, '”')

  # Second bit: Remove spaces before colons, semicolons, commas, ?s, )s
  colon_semicolon_comma_count += text.scan(/(\w)\s+([:;,\)])/).count
  text.gsub!(/(\w)\s+([:;,\?])/, '\1\2')

  # Third bit: Insert a space between comma-separated words without a space
  comma_space_count += text.scan(/(\w),(\w)/).count
  text.gsub!(/(\w),(\w)/, '\1, \2')

  # Fourth bit: Insert a space after a comma, before a year
  comma_before_year_count += text.scan(/(\w),(\d{4})/).count
  text.gsub!(/(\w),(\d{4})/, '\1, \2')

  # Fifth bit: Replace period followed by one or more spaces and a hyphen with period and a single space
  period_space_hyphen_count += text.scan(/\.\s+\-/).count
  text.gsub!(/\.\s+\-/, '. ')

  # Misc: just delete any • or ■ spotted
  text.gsub!(/(\•|\■)/, '')

  # New logic: Convert ‘ ‘ or ‘ ‘ to “
  single_to_double_quote_count += text.scan(/‘\s?‘/).count
  text.gsub!(/‘\s?‘/, '“')

  # New rule: Replace period followed by one or more spaces and any character with period and that character without newline
  period_space_correction_count += text.scan(/\.\s+.\n/).count
  text.gsub!(/(\.)\s+(.)\n/, ".\n")

  # Update total_corrections to include the new counter
  total_corrections = curly_quotes_count + colon_semicolon_comma_count + comma_space_count + comma_before_year_count + period_space_hyphen_count + single_to_double_quote_count + period_space_correction_count

  # Report completed work
  puts "#{total_corrections} total.\n  (Curly quotes: #{curly_quotes_count},\n  Colon/Semicolon/Comma: #{colon_semicolon_comma_count},\n  Comma-Space: #{comma_space_count},\n  Comma-Year: #{comma_before_year_count},\n  Period-Space-Hyphen: #{period_space_hyphen_count},\n  Single to Double Quote: #{single_to_double_quote_count},\n  Period Space Correction: #{period_space_correction_count})."

  # Split the modified text back into lines and return
  text.split("\n")
end

#NOTE: does not capture all captions, as some have lowercase bits.
def remove_uppercase_plus_period_captions(cleaned_lines)
  print "Uppercase captions removed: "
  text = cleaned_lines.join("\n")
  caption_removal_count = text.scan(/\n\s*[A-Z\:\-\'\"\’\, ]{7,}\.\s*\n/).count
  text.gsub!(/\n\s*[A-Z\:\-\'\"\’\, ]{7,}\.\s*\n/, "\n\n")
  puts "#{caption_removal_count}"
  text.split("\n")
end


# Delete headers.
def remove_headers(lines)
  print "Removing headers: "
  cleaned_lines = []
  count = 0
  # Define patterns for deletion
  patterns = [
    # Header pattern #1: like Abb, 01d, and Zech
    /^[\w\d]{3,4}$/,
    # Header pattern #2: like (1), (253), ( 3. ), (' 92 )
    /^\(.?\s*[\w\d]{1,4}\.?\s*\.?\)$/,
    # Header pattern #3: like ( 3*4 ), (6^3)
    /^\(\s*[\d\w]{1,2}.[\d\w]{1,2}\s*\)$/,
    # Header pattern #4: like 'Adv ( i' and '2 ) 2EI'
    /^[\w\d]{1,4}\s*[\(\)]\s*[\w\d]{1,4}$/,
    # Header pattern #5: like '( 13 )  Aga'
    /^\(\s*[\w\d]{1,4}\s*\)\s*[\w\d]{3,4}$/,
    # Header pattern #6: ( i
    /^\(\s*[\w\d]{1,4}$/,
    # Header pattern #7: 7), 2 )
    /^[\w\d]{1,4}\s*\)$/,
    # Header pattern #8: Aga  ( 14 ). Ago and Apo ( 44 )  Apo
    /^[\w\d]{1,4}\s*\(\s*[\w\d]{1,4}\s*\)\.?\s*[\w\d]{1,4}$/,
    # Header pattern #9: I T33 )
    /^[\w\d]\s*[\w\d]{3,4}\s*\)$/
  ]
  # Remove header lines according to matching patterns
  lines.each do |line|
    if patterns.any? {|pattern| line =~ pattern }
      line = ''
      count += 1
    end
    cleaned_lines << line
  end
  puts count
  cleaned_lines
end

def remove_column_and_page_breaks(cleaned_lines)
  cleaned_lines.map!(&:rstrip)
  # Join the lines into a single string with newlines
  text = cleaned_lines.join("\n")
  print "Removing column and page breaks: "

  # Define the pattern to match words split by hyphens across multiple newlines
  hyphen_pattern = /(\w+)-\s*\n{2,}\s*(\w+)/m

  # Perform the replacement for hyphenated words across multiple lines
  hyphen_break_count = text.scan(hyphen_pattern).size
  text = text.gsub(hyphen_pattern) { "#{$1}#{$2}" }

  # Now, replace 3 or 4 consecutive newline characters with two newlines
  newline_replacement_count = text.scan(/\n{3,}/).size
  text = text.gsub(/\n{3,}/, "\n\n")

  puts "\n  #{hyphen_break_count + newline_replacement_count} replacements made."

  # Split the cleaned text back into lines to return an array
  text.split("\n")
end

def combine_broken_sentences(cleaned_lines)
  cleaned_lines.map!(&:rstrip)
  # Join the lines into a single string with newlines
  text = cleaned_lines.join("\n")
  print "Rejoining (some) broken sentences: "

  # Match broken sentences, including those ending with ',', ':', ';', '-'
  # (some of the latter will still be left) and followed by a lowercase
  # letter after any number of newlines.
  pattern = /(\w+\.?[,:;]?)\s*\n+\s*(\“?[a-z]+)/m
  # NOTE: this will probably wrongly fix some legit hyphenations that occur
  # at column and page breaks. Needs to use a dictionary, probably. OK for now.
  hyphen_pattern = /(\w+)\-\s*\n+\s*([a-z]+)/m

  # New pattern to match and combine lines ending with a lowercase letter
  # followed by one or two newlines, then starting with lowercase letter
  lowercase_pattern = /([a-z])\n{1,2}([a-z])/m

  # Use the 'm' modifier for multiline mode, allowing '.' to match newlines
  # Count occurrences before replacement for reporting
  count = text.scan(pattern).size
  hyphen_count = text.scan(hyphen_pattern).size
  lowercase_count = text.scan(lowercase_pattern).size

  # Perform the replacements
  text = text.gsub(pattern) { "#{$1} #{$2}" }
  text = text.gsub(hyphen_pattern) { "#{$1}#{$2}" }
  text = text.gsub(lowercase_pattern) { "#{$1} #{$2}" }

  puts "\n  #{count} rejoined;\n  #{hyphen_count} hyphens fixed;\n  #{lowercase_count} lowercase breaks fixed."

  # Split the cleaned text back into lines to return an array
  text.split("\n")
end


def fix_bad_curly_braces(cleaned_lines)
  print "Fixing bad curly braces: "
  count = 0

  # Define the regex pattern to match and fix bad curly braces
  pattern = /\{([A-Za-z\s,.:;'-]{0,15})\)|\(([A-Za-z\s,.:;'-]{0,15})\}/

  # Iterate through each line and apply the regex replacement
  cleaned_lines.map! do |line|
    if line =~ pattern
      count += 1
      # Replace both types of mismatches with regular parentheses
      line.gsub(pattern) { '(' + ($1 || $2) + ')' }
    else
      line
    end
  end

  puts count
  cleaned_lines
end

def write_to_html(cleaned_lines)
  File.open($output_path, 'w') do |file|
    cleaned_lines.each do |line|
      file.puts "#{line.strip}"
    end
  end
end

# Process the file
cleaned_lines = prep_array
cleaned_lines = remove_uppercase_plus_period_captions(cleaned_lines)
cleaned_lines = fix_selected_punctuation(cleaned_lines)
cleaned_lines = fix_bad_curly_braces(cleaned_lines) if $volume_code == 'ccrk'
cleaned_lines = remove_headers(cleaned_lines) if $volume_code == 'ccrk'
cleaned_lines = remove_column_and_page_breaks(cleaned_lines)
cleaned_lines = combine_broken_sentences(cleaned_lines)
cleaned_lines = fix_selected_punctuation(cleaned_lines)  # Again!
write_to_html(cleaned_lines)
