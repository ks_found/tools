require 'nokogiri'
require 'fileutils'
require 'tate'

# Globals
$volume_code = 'ccrk'
$input_filename = "#{$volume_code}-mu.html"
$output_directory = $volume_code.dup
$html_directory = File.join($output_directory, '/html')
$input_path = File.join($output_directory, $input_filename)

# Ensure the HTML output directory exists
FileUtils.mkdir_p($html_directory) unless Dir.exist?($html_directory)

# Read the input file
input_html = File.read($input_path)

# Parse the HTML
doc = Nokogiri::HTML(input_html)

# Helper method to refine title for the <title> tag
def refine_title_for_html_title_tag(title)
  # Remove apostrophes not in contractions or possessive forms
  refined_title = title.gsub(/(?<!\w)'(?!\w|s\b)/, '') # Lookbehind for a non-word character, lookahead for a non-word character or 's' at a word boundary
  # Retain apostrophes in contractions and possessive forms
  refined_title.gsub(/([a-zA-Z])'([a-zA-Z])/, '\1\2') # Simplified contraction handling
end

# Gets article title, replaces spaces and other disallowed chars with '_'.
# Does *not* append '.zwi'.
def name_zwi(title)
  # Remove characters that shouldn't be in URLs or ZWI names.
  title.gsub!(/[ \/\|\?\%\*\:\|\'\"\<\>\.\,\;\=\′]/, "_")
  # Let Tate transliterate diacritical forms to ASCII forms.
  title = Tate::transliterate(title)
  # Do it again after Tate makes its changes, in case Tate outputs '?' or
  # anything else that shouldn't be in there.
  title.gsub!(/[ \/\|\?\%\*\:\|\'\"\<\>\.\,\;\=\′]/, "_")
  # Remove any doubled underscores.
  title.gsub!(/__/, '_')
  # Remove preceding/concluding '_' which looks bad.
  title = title.chomp('_').reverse.chomp('_').reverse
  # Remove preceding/concluding '-' which looks bad.
  title = title.chomp('-').reverse.chomp('-').reverse
  title
end

# Initialize a hash to track title occurrences
title_counts = Hash.new(0)

# Process each <article> tag
doc.css('article').each do |article|
  original_title = article.css('.title strong').text.strip
  title = original_title.dup # Duplicate the title to modify for filename
  next if title.empty?
  refined_html_title = refine_title_for_html_title_tag(original_title) # Refine title for <title> tag

  # Increment the title count and modify title if necessary
  title_counts[refined_html_title] += 1
  title = "#{title} (#{title_counts[refined_html_title]})" if title_counts[refined_html_title] > 1

  # Create a valid filename from the title
  filename = name_zwi(title)
  file_path = File.join($html_directory, "#{filename}.html")

  # Create the HTML content for the file
  html_content = <<-HTML
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="generator" content="AutoZWI, by the Knowledge Standards Foundation">
    <title>#{refined_html_title}</title>
</head>
<body>
    #{article.to_html}
</body>
</html>
  HTML

  # Write to the file
  File.write(file_path, html_content)
end
