# Globals: use to reset the name of the working file.
$volume_code = 'ccrk'
$input_filename = "#{$volume_code}-cu.txt"
$output_filename = $input_filename.gsub('-cu.txt', '-mu.html')
$output_directory = $input_filename.gsub('-cu.txt', '')
$input_path = "#{$output_directory}/#{$input_filename}"
Dir.mkdir($input_path) unless Dir.exist?($output_directory)
unless Dir.exist?($output_directory)
  Dir.mkdir($output_directory)
  puts "Created #{$output_directory}."
end


# Function to read the file and return an array of lines
def read_file
  File.readlines($input_path).map(&:chomp)
end

# Function to score a line based on heuristics
def score_line(line, prev_line, prev_prev_line, current_letter)
  # Only score lines that start with the current letter
  if line.start_with?(current_letter)
    score = 30 # Base score for matching the current letter
    # Trim whitespace from the end of prev_line before checking conditions
    trimmed_prev_line = prev_line.rstrip

    if $volume_code == 'ccrk_old'
      score += 50 if
        trimmed_prev_line[-1] == '.' ||
        trimmed_prev_line[-2..-1] == '.)' ||
        trimmed_prev_line.length <= 12 ||
        (trimmed_prev_line.empty? && prev_prev_line.empty?)
    else
      # For volumes other than 'ccrk_old', adjust scoring criteria as needed
      score += 50 if
        prev_line[-1] == '.' ||
        (!trimmed_prev_line.nil? and trimmed_prev_line[-2..-1] == '.)') ||
        (prev_line.empty? && prev_prev_line.empty?)
    end

    return score
  end

  # Return nil for lines that don't start with the current letter
  nil
end

def omit_line?(line, prev_line, next_line, avg_length)
  # Direct check for blank lines following the candidate
  return true if next_line.strip.empty? && prev_line[0]&.match(/[a-z]/) && ($volume_code == 'ccrk_old' && prev_line.length >= avg_length)

  # Existing conditions for omitting lines based on $volume_code
  return true if line.length <= 4 && !line.end_with?('.')

  if $volume_code == 'ccrk_old'
    # Apply line length based conditions only for 'ccrk_old'
    uppercase_count = line.scan(/[A-Z]/).length
    total_count = line.length
    uppercase_percentage = uppercase_count.to_f / total_count
    return true if uppercase_percentage >= 0.7
  end

  false
end

# Calculate the average line length excluding blank lines
def average_line_length(lines)
  non_blank_lines = lines.reject { |line| line.strip.empty? }
  total_length = non_blank_lines.sum(&:length)
  total_length.to_f / non_blank_lines.size
end

def extract_key_term(line)
  # Strip double quotation marks for all processing
  cleaned_line = line.strip.gsub(/(\"|\“|\”|\‘ |\‘)/, '')
  # Strip abbreviatiatory periods
  abbrevs = 'St|Dr'
  cleaned_line = line.strip.gsub(/(#{abbrevs})\.(\s{1,2}[A-Z]\w+)/) { "#{$1}#{$2}" }

  # Rule for standard short cross-references. Early because very specific and predictable.
  if cleaned_line =~ /^(.{1,40})\.\s+See .{1,40}\.\s*$/
    return $1
  end

  # Short ccrk quotations of other references take a specific format and
  # indicate the article title clearly.
  if $volume_code == 'ccrk' # Seems specific to ccrk
    if line =~ /^([^“]{1,40})\.\s+“[^”]*”\s*[—–][^\.]{0,40}\.$/
      return $1.strip
    end
  end

  # Rule for academic titles or similar indicators
  if cleaned_line =~ /^(.{1,40}?)(,|;|\s*\()\s*(D\.\s*?D\.|S\.\s*?T\.\s*?D\.|b\.|a\.\s*d\.)/
    return $1.gsub(/[^a-zA-Z\s]/, '').strip
  end

  # Rule for full names with two word-comma or word-semicolon combinations
  if cleaned_line =~ /^([A-Z][\w\s’']*\, [A-Z][\w\s’']*(,|;)) \“?[a-z]/
    # Ensure commas or semicolons at the end of keywords are not included
    keyword = $1.gsub(/[,;]+$/, '').strip
    return keyword.gsub(/[^a-zA-Z\s]/, '')
  end

  # Rule for general identification; before single commas (late applied).
  if cleaned_line =~ /^([A-Z][\w\s’']{1,30}), [a-z]/
    return $1.gsub(/[^a-zA-Z\s]/, '').strip
  end

  # Rule for handling phrases ending with "is" and "denotes"
  if cleaned_line =~ /^([\w\s’']{1,30}?)(?=\s{1,2}(denote|denotes|is|means)\b)/
    return $1.strip
  end

  # Rule for general identification; before periods (late applied).
  if cleaned_line =~ /^([A-Z][\w\s’']{1,50}?)\. [A-Z]/
    return $1.gsub(/[^a-zA-Z\s]/, '').strip
  end

  # Default extraction if none of the specific rules above match
  first_word = cleaned_line.split(/\s/).first
  first_word ? first_word.gsub(/[^a-zA-Z]/, '') : ""
end


def apply_exceptions(scores, line_key_term_pairs)
  # Load inclusion and exclusion lists
  inclusions = File.readlines("#{$volume_code}/inclusions.txt").map(&:chomp)
  exclusions = File.readlines("#{$volume_code}/exclusions.txt").map(&:chomp)

  line_key_term_pairs.each_with_index do |pair, index|
    line = pair[:line]

    # Apply max score for inclusions
    if inclusions.any? { |inc| line.include?(inc) }
      scores[index] = 100
    end

    # Apply min score for exclusions
    if exclusions.any? { |exc| line.include?(exc) }
      scores[index] = 0
    end
  end
end

def evaluate_alignment_and_score(lines, output_filename='key_terms_with_initial_scores.txt')
  scores = Array.new(lines.size, 0) # Initialize scores with 0 for all lines
  key_terms = [] # Initialize an array to store key terms

  current_letter = 'A'

  File.open(output_filename, 'w') do |file|
    line_key_term_pairs = lines.map.with_index do |line, index|
      key_term = extract_key_term(line)
      key_terms << key_term
      { line: line, key_term: key_term, index: index }
    end

    # Actually assign scores based on alpha order + exceptions
    line_key_term_pairs.each_with_index do |current_pair, index|
      # Initial penalty flag set to false
      penalty_applied = false

      # Skip lines that do not start with the current letter
      next unless current_pair[:key_term].start_with?(current_letter)

      # Examine a 3-line window first
      [-1, 1].each do |offset|
        window = line_key_term_pairs[[0, index + offset - 1].max..[index + offset + 1, line_key_term_pairs.size - 1].min].select { |pair| pair[:key_term].start_with?(current_letter) }
        sorted_terms = window.map { |pair| pair[:key_term] }.sort
        current_order = window.map { |pair| pair[:key_term] }

        if sorted_terms != current_order
          # Remove the current term and check the order again
          sorted_terms.delete(current_pair[:key_term])
          current_order.delete(current_pair[:key_term])
          if sorted_terms == current_order
            scores[index] -= 20
            penalty_appplied = true
          end
        end
      end

      # Now, examine a 5-line window with the same letter check
      window = line_key_term_pairs[[0, index - 2].max..[index + 2, line_key_term_pairs.size - 1].min].select { |pair| pair[:key_term].start_with?(current_letter) }
      sorted_terms = window.map { |pair| pair[:key_term] }.sort
      current_order = window.map { |pair| pair[:key_term] }

      if sorted_terms != current_order
        # Remove the current term and check the order again
        sorted_terms.delete(current_pair[:key_term])
        current_order.delete(current_pair[:key_term])
        if sorted_terms == current_order
          # Apply a more severe penalty for larger context disruption
          scores[index] -= 30
          penalty_applied = true
        end
      end

      # If no penalties were applied, increase the score by 30
      scores[index] += 30 unless penalty_applied
    end

    apply_exceptions(scores, line_key_term_pairs)

  end

  { scores: scores, key_terms: key_terms }
end

$line_scores = {}

def find_previous_content_line(lines, start_index)
  index = start_index - 1
  while index >= 0
    line = lines[index].strip
    # Return this line if it's not empty and not a marker
    return lines[index] if !line.empty? && line !~ /\[\[[A-Z]\]\]/
    index -= 1
  end
  "" # Return an empty string if no suitable line is found
end

def assign_previous_lines(text_lines, current_index)
  # Find the previous line that is not empty or a marker
  prev_line_index = current_index - 1
  prev_line = find_previous_content_line(text_lines, prev_line_index + 1)

  # Start the search for prev_prev_line from the position where prev_line was found
  prev_prev_line_index = text_lines.index(prev_line) - 1
  prev_prev_line = find_previous_content_line(text_lines, prev_prev_line_index + 1) unless prev_prev_line_index.nil?

  [prev_line, prev_prev_line]
end

def process_text
  text_lines = read_file()
  avg_length = average_line_length(text_lines)
  result = evaluate_alignment_and_score(text_lines)

  # Accessing the scores and key terms
  alignment_scores = result[:scores]
  key_terms = result[:key_terms]

  # Define output file paths within the specified output directory
  output_file_path = "#{$output_directory}/output.txt"
  low_scorers_file_path = "#{$output_directory}/low_scorers.txt"
  high_scorers_file_path = "#{$output_directory}/high_scorers.txt"
  current_letter = 'A'

  File.open(output_file_path, "w") do |output_file|
    File.open(low_scorers_file_path, "w") do |low_scorers_file|
      File.open(high_scorers_file_path, "w") do |high_scorers_file|        text_lines.each_with_index do |current_line, index|
          if current_line =~ /\[\[([A-Z])\]\]/
            current_letter = $1
            next
          end

          next_line = text_lines[index + 1] || ""
          next if omit_line?(current_line, text_lines[index - 1] || "", next_line, avg_length)

          # Skip previous (or previous previous) line(s) if of [[A]]..[[Z]]
          # form or if an empty string.
          prev_line = ""
          prev_prev_line = ""
          prev_line, prev_prev_line = assign_previous_lines(text_lines, index)

          initial_score = score_line(current_line, prev_line, prev_prev_line, current_letter)
          adjusted_score = initial_score + (alignment_scores[index] || 0) if initial_score

          # Update $line_scores with the adjusted score for each line
          $line_scores[index] = adjusted_score if adjusted_score

          if adjusted_score && adjusted_score < 60
            # Output to low_scorers.txt with context
            low_scorers_file.puts("#{adjusted_score}:")
            low_scorers_file.puts(">>> #{current_line} <<<")
            low_scorers_file.puts("") # Separation
          elsif adjusted_score
            output_file.puts("#{adjusted_score}: #{current_line}")

            if adjusted_score >= 60
              # Logic to include only relevant subsequent lines for high scorers
              high_scorers_file.puts("#{current_line}")
              (1..3).each do |n|
                next_index = index + n
                break if next_index >= text_lines.length

                next_line_score = score_line(text_lines[next_index], current_line, text_lines[index - 1] || "", current_letter)
                break if next_line_score  # Stop if the next line is a high scorer itself

                high_scorers_file.puts("#{text_lines[next_index]}")
              end
              high_scorers_file.puts("") # Separation
            end
          end
        end
      end
    end
  end
  key_terms
end

# This method processes each line of the input text, wrapping identified titles within articles
# with <span class="title">...</span> tags based on their significance scores and key terms.
# Additionally, it logs the final scores of each line for review or testing purposes.
def mark_up(key_terms)
  text_lines = read_file  # Reads the input text lines
  output = "#{$output_directory}/#{$output_filename}"

  File.open(output, "w") do |file|
    # Iterates through each line of the text to process articles and titles.
    text_lines.each_with_index do |line, index|
      next if line =~ /\[\[([A-Z])\]\]/  # Skips marker lines indicating the start of a new alphabetical section.

      if $line_scores[index] && $line_scores[index] >= 60  # Checks if the current line is the start of a new article based on its score.
        file.puts("</article>") if index > 0  # Closes the previous article tag if not the first line.
        file.puts("<article>")  # Starts a new article tag.

        current_key_term = key_terms[index]  # Retrieves the key term for the current line.
        matched_title = nil  # Initializes matched_title to nil.

        # Dynamically checks each portion of the line to find a match with the current key term.
        (current_key_term.length..line.length).each do |length|
          portion_of_line = line[0, length]
          extracted_key_term = extract_key_term(portion_of_line)
          if extracted_key_term == current_key_term
            matched_title = portion_of_line  # Sets matched_title when a match is found.
            break
          end
        end

        if matched_title
          matched_start_pos = line.index(matched_title)  # Finds the start position of the matched title.

          if matched_start_pos
            # Inserts the opening span tag before the matched title.
            line.insert(matched_start_pos, "<span class=\"title\"><strong>")

            # Initially sets the position to insert the closing span tag after the matched title plus a buffer.
            insert_pos_base = matched_start_pos + current_key_term.length + "<span class=\"title\"><strong>".length
            if current_key_term.include? "A Becket"
              puts current_key_term
              puts insert_pos_base
            end
            search_start_pos = insert_pos_base
            search_start_pos = [search_start_pos, line.length].min  # Ensures it does not exceed the line length.

            if current_key_term.length >= 3
              end_chars = current_key_term[-3..]  # Extracts the last three characters of the key term.
              # Adjusts search to look for the last occurrence of the key term's end characters within a reasonable range.
              search_range_end = [search_start_pos + current_key_term.length, line.length].min
              last_occurrence_pos = line.rindex(end_chars, search_range_end)
              if last_occurrence_pos
                insert_pos = last_occurrence_pos + end_chars.length  # Adjusts insert_pos based on the last occurrence found.
              else
                insert_pos = insert_pos_base  # Falls back to the base position if no occurrence is found.
              end
            else
              insert_pos = insert_pos_base  # Falls back to the base position if the key term is too short.
            end

            line.insert(insert_pos, "</strong></span>")  # Inserts the closing span tag at the calculated position.
          end
        end
      end

      file.puts(line)  # Writes the processed line to the output file.
    end
    file.puts("</article>")  # Closes the last article tag at the end of the file.
  end

  # Log the final scores for each line after processing the text.
  File.open('lines_with_final_scores.txt', 'w') do |f|
    key_terms.each_with_index do |key_term, index|
      line = text_lines[index]  # Retrieve the corresponding line from the input text array
      final_score = $line_scores[index]  # Retrieve the final score for the line from the global array
      # Log the final score along with the key term and a snippet of the original line for review or testing
      f.puts "#{final_score.to_s.ljust(5)} | #{key_term.ljust(20)} | #{line[0, 40]}"
    end
  end
end

# Wraps paragraphs in <p>...</p> tags in the processed file, excluding <article> and </article> lines.
def add_paragraphs
  input_path = "#{$output_directory}/#{$output_filename}"  # Assumes the file path from mark_up
  temp_path = "#{input_path}.temp"  # Temporary file path for intermediate changes

  File.open(temp_path, "w") do |temp_file|
    File.foreach(input_path) do |line|
      line.chomp!  # Removes trailing newline for processing

      # Check if the line is not empty and not an article tag
      if !line.empty? && !line.match?(/<\/?article>/)
        temp_file.puts "<p>#{line}</p>"  # Wrap the line with <p> tags
      else
        temp_file.puts line  # Write the line as-is for article tags
      end
    end
  end

  # Replace the original file with the modified content
  File.rename(temp_path, input_path)
end


def post_process_mu_file()
  output = "#{$output_directory}/#{$output_filename}"
  content = File.read(output)

  # Replace the specified pattern with `</article>\n`
  modified_content = content.gsub(/[\n\s*]+\<\/article\>/, "\n</article>\n")

  # Write the modified content back to the file
  File.write(output, modified_content)
end

# Actually call functions
key_terms = process_text
mark_up(key_terms)
add_paragraphs
post_process_mu_file
