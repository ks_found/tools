#!/usr/bin/ruby

# Get dictionary.
dict = File.readlines("words.txt")
puts "works!" if dict.include?("bees")

# Get working directory from user-supplied argument.
# Testing with /home/globewalldesk/Nextcloud/OEDP/OhioEncyclopedia
dir = ""
if ARGV[0] == nil
  puts "Please specify directory as argument."
else
  dir = ARGV[0]
end

# Get list of image files.
begin
  image_paths = Dir["#{dir}/*.{jpg,jpeg,png,tif,tiff}"]
rescue Exception => e
  puts "Looks like dir isn't a path that contains image files:\n#{e}"
end

# Sort them by number just before extension.
image_paths.sort_by! do |path|
  path.match(/([0-9]+)\.(jpg|jpeg|png|tif|tiff)/)
  $1.to_i
end

texts = []                    # Array of Tesseract text output.
# Run Tesseract on each image; put in array.
# NOTE: 16..18 is for testing!
image_paths[0..95].each do |path|
  texts << `tesseract "#{path}" stdout`
end
# Stitch together.
text = texts.join("\n\n")
puts text

puts "\n\n=====================\n\n"

# Iterate through matches of "FOO-\nBAR", i.e., hyphenated words with newline.
hyphenated = []
unhyphenated = []
text.scan(/(\w+)\-\n(\w+)/) do |word1, word2|
  # Replace "[word1]-\n[word2]" with "word1-word2" if hyphenated word is found
  # in dictionary.
  if dict.include? "#{word1}-#{word2}"
    hyphenated << "#{word1}-\n#{word2}"
  else
    unhyphenated << "#{word1}-\n#{word2}"
  end
end
# Actually perform the substitutions.
# This looks for the hyphenations (with newlines) to deal with. If the English
# word is supposed to be hyphenated, the hyphen is left in; otherwise, not.
hyphenated.each {|w| text.gsub!(w, w.tr("-\n", "-"))}
unhyphenated.each {|w| text.gsub!(w, w.tr("-\n", ""))}


# Replace "\n\n" and "\n\s+\n" with "WXYZ".
text.gsub!(/(\n\n|\n\s+\n)/, "WXYZ")
text.gsub!("\n", " ")
text.gsub!("WXYZ", "\n\n")

# otherwise replace with "word1word2".
# Write output to file.
puts text
File.write("First96Tess.txt", text)
