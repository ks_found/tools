tinymce.init({
    selector: "#writingArea",
    menubar: false,
    statusbar: false,
    nowrap: false,
    save_onsavecallback: save,
    save_enablewhendirty: false,
    plugins: "save searchreplace lists image link",
    toolbar: "save | fontfamily fontsize | bold italic underline strikethrough | bullist numlist | image link hr | superscript subscript | backcolor forecolor | indent outdent | alignleft aligncenter alignright alignjustify | removeformat",
    font_family_formats: "Open Sans=open sans,sans-serif; Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats"
});

function save() {
    console.log(tinymce.activeEditor.getContent());
}
