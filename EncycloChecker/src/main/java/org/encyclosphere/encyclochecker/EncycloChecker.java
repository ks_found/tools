package org.encyclosphere.encyclochecker;

import jakarta.mail.*;
import jakarta.mail.internet.AddressException;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpConnectTimeoutException;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class EncycloChecker {

    static HashMap<String, String> testQueries = new HashMap<>();

    static ArrayList<String> noReaderPublishers = new ArrayList<>(),
                            noResultsEmailsSent = new ArrayList<>(),
                             noReaderEmailsSent = new ArrayList<>();

    static Session session;

    static InternetAddress sendingAddress, receivingAddress;

    private static String username, password;

    private static final boolean VERBOSE_LOGGING = false;

    public static void main(String[] args) throws IOException, AddressException {
        JSONObject config = new JSONObject(Files.readString(Path.of("config.json")));

        JSONObject testQueriesObject = config.getJSONObject("testQueries");
        for(Map.Entry<String, Object> entry : testQueriesObject.toMap().entrySet()) {
            testQueries.put(entry.getKey(), ((String) entry.getValue()).replace(" ", "%20"));
        }

        noReaderPublishers = new ArrayList<>(config.getJSONArray("noReaderPublishers").toList().stream().map(Object::toString).toList());

        JSONObject email = config.getJSONObject("email");
        username = email.getString("username");
        password = email.getString("password");

        Authenticator authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        };

        final Properties props = System.getProperties();
        props.setProperty("mail.smtp.host", email.getString("host"));
        props.setProperty("mail.smtp.port", email.getString("port"));
        props.setProperty("mail.smtp.auth", "true");
        session = Session.getDefaultInstance(props, authenticator);

        sendingAddress = new InternetAddress(email.getString("username"));
        receivingAddress = new InternetAddress(email.getString("receivingAddress"));

        HttpClient client = HttpClient.newBuilder().build();
        ScheduledExecutorService service = Executors.newScheduledThreadPool(1);
        service.scheduleAtFixedRate(() -> {
            try {
                for(Map.Entry<String, String> testQuery : testQueries.entrySet()) {
                    String publisher = testQuery.getKey(), query = testQuery.getValue();
                    if(VERBOSE_LOGGING) {
                        System.out.println();
                        System.out.println("TESTING: " + publisher);
                    }
                    HttpResponse<String> response = client.send(request("https://encyclosearch.org/encyclosphere/search?q=" + query + "&includedPublishers=" + publisher), HttpResponse.BodyHandlers.ofString());
                    JSONObject responseObject = new JSONObject(response.body());

                    if(responseObject.getInt("ResultsCount") < 1 && !noResultsEmailsSent.contains(publisher)) {
                        sendEmail("Problem with EncycloSearch", publisher + " isn't returning results.");
                        noResultsEmailsSent.add(publisher);
                        continue;
                    }

                    if(!noReaderPublishers.contains(publisher)) {
                        String url = responseObject.getJSONArray("Results").getJSONObject(0).getString("SourceURL");
                        HttpResponse<String> articleResponse = client.send(request("https://encyclosearch.org/reader?url=" + encodeURIComponent(url)), HttpResponse.BodyHandlers.ofString());
                        if((articleResponse.statusCode() != 200 || articleResponse.body().length() < 11000) && !noReaderEmailsSent.contains(publisher)) {
                            sendEmail("Problem with EncycloSearch", "Reader isn't working for " + publisher + ".");
                            noReaderEmailsSent.add(publisher);
                            continue;
                        }

                        if(VERBOSE_LOGGING) System.out.println("PASSED");
                    }
                }
            } catch(HttpConnectTimeoutException timeoutException) {
                sendEmail("EncycloSearch is down!", "EncycloSearch is not responding! The test search timed out.");
            } catch(Exception e) {
                e.printStackTrace();
            }
        }, 0, 5, TimeUnit.MINUTES);

        service.scheduleAtFixedRate(() -> {
            noResultsEmailsSent.clear();
            noReaderEmailsSent.clear();
        }, 1, 1, TimeUnit.DAYS);
    }

    static HttpRequest request(String url) throws URISyntaxException {
        return HttpRequest.newBuilder()
                .uri(new URI(url))
                .timeout(Duration.ofSeconds(10))
                .build();
    }

    static void sendEmail(String subject, String body) {
        if(VERBOSE_LOGGING) System.out.println("FAIL!");
        System.out.println("SENDING EMAIL TO " + receivingAddress.getAddress() + ":");
        System.out.println(subject);
        System.out.println(body);
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(sendingAddress);
            message.addRecipient(Message.RecipientType.TO, receivingAddress);
            message.setSubject(subject);
            message.setText(body);
            Transport.send(message);
            System.out.println("Message sent");
            System.out.println();
        } catch(MessagingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Encodes the passed String as UTF-8 using an algorithm that's compatible
     * with JavaScript's <code>encodeURIComponent</code> function. Returns
     * <code>null</code> if the String is <code>null</code>.
     *
     * @param s The String to be encoded
     * @return the encoded String
     * @author John Topley
     * @see <a href="https://stackoverflow.com/a/611117/5905216">Source of method</a>
     */
    public static String encodeURIComponent(String s) {
        String result;
        result = URLEncoder.encode(s, StandardCharsets.UTF_8)
                .replaceAll("\\+", "%20")
                .replaceAll("%21", "!")
                .replaceAll("%27", "'")
                .replaceAll("%28", "(")
                .replaceAll("%29", ")")
                .replaceAll("%7E", "~");

        return result;
    }

}
