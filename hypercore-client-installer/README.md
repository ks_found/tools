# hypercore-client-installer
Installs `hyp`, the Hypercore Protocol CLI, on Debian-based Linux distros. Does not require `sudo`.

## Usage
```
curl -sSL https://gitlab.com/ks_found/tools/-/raw/main/hypercore-client-installer/hypercore-client-installer.sh | bash
```

To copy the command to your clipboard, hover over the box, and click the button in the top right.
