#!/bin/bash
echo
echo '## Installing nvm & node v14...'
echo
curl -sSL https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
nvm install 14
nvm use 14

echo
echo '## Installing hyp...'
echo
npm i -g @hyperspace/cli hyperspace hyperspace-mirroring-service

echo
echo '## Starting daemon...'
echo
nohup hyperspace > /dev/null 2>&1 &
nohup hyperspace-mirror --port 3282 > /dev/null 2>&1 &
hyp daemon start

echo
echo 'Done! To use hyp, restart your terminal.'
echo 'hyp will stop working after a reboot. To make it work again, run these commands after rebooting:'
echo 'nohup hyperspace > /dev/null 2>&1 &'
echo 'nohup hyperspace-mirror --port 3282 > /dev/null 2>&1 &'
echo 'hyp daemon start'
echo
