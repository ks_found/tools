package org.encyclosphere.imageusagefinder;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class ImageUsageFinder {

    // Change this number to change the sample size.
    static final int IMAGES_TO_PROCESS = 1000;

    static int imagesProcessed = 0;
    static LinkedHashMap<String, Integer> usageCountMap = new LinkedHashMap<>();

    public static void main(String[] args) {

        // Query Wikimedia Commons every 500ms
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try {

                    // If the required number of images have been processed, exit the loop
                    if(imagesProcessed >= IMAGES_TO_PROCESS) {
                        this.cancel();
                        return;
                    }

                    // Get a random file from Wikimedia Commons
                    Document document = Jsoup.connect("https://commons.wikimedia.org/wiki/Special:Random/File").userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36").get();

                    // Check if the file is used by English Wikipedia
                    Elements usageList = document.select("#mw-imagepage-section-globalusage .mw-gu-onwiki-en_wikipedia_org ul li a");
                    if(usageList.size() == 0 || document.select("#mw-imagepage-nolinkstoimage").size() > 0) return;

                    List<String> usages = usageList.stream().map(Element::text).filter(text -> !text.contains(":")).toList();
                    if(usages.size() == 0) return;

                    // If the file is used by English Wikipedia, get its filename and the number of times it's used
                    String filename = document.baseUri().split("File:")[1].replace("_", " ");

                    // Print some information
                    System.out.print("\n\n");
                    if(imagesProcessed > 0) System.out.println(getResults());
                    System.out.println(usages.size() + " usage(s) of \"" + filename + "\" in English Wikipedia:");
                    for(String usage : usages) System.out.println(usage);
                    System.out.println((imagesProcessed + 1) + "/" + IMAGES_TO_PROCESS + " image(s) processed");

                    // Add the filename and the number of usages to the map; increment the counter that keeps track of the number of images processed
                    usageCountMap.put(filename, usages.size());
                    imagesProcessed++;
                } catch(IOException e) {
                    e.printStackTrace();
                }
            }
        }, 500, 500);
    }

    static String getResults() throws IOException {

        // Get the number of times each image is used
        Collection<Integer> usageCounts = usageCountMap.values();

        // Create a file, usageCounts.txt, containing a comma-separated list of the number of times each image is used
        StringBuilder usageCountBuilder = new StringBuilder();
        for(Integer usageCount : usageCounts) usageCountBuilder.append(usageCount).append(", ");
        String usageCountText = usageCountBuilder.toString();
        Files.writeString(Paths.get("usageCounts.txt"), (usageCountText.length() > 2 ? usageCountText.substring(0, usageCountText.length() - 2) : usageCountText));

        // Create a file, usageList.txt, containing the filename of each image and the number of times it's used
        StringBuilder usageListBuilder = new StringBuilder();
        int totalNumUsages = 0;
        int largestNumUsages = 0;
        ArrayList<String> filesWithLargestUsages = new ArrayList<>();
        for(Map.Entry<String, Integer> usageEntry : usageCountMap.entrySet()) {
            totalNumUsages += usageEntry.getValue();
            if(usageEntry.getValue() == largestNumUsages) {
                filesWithLargestUsages.add(usageEntry.getKey());
            } else if(usageEntry.getValue() > largestNumUsages) {
                largestNumUsages = usageEntry.getValue();
                filesWithLargestUsages.clear();
                filesWithLargestUsages.add(usageEntry.getKey());
            }
            usageListBuilder.append(usageEntry.getKey()).append(": ").append(usageEntry.getValue().intValue()).append("\n");
        }
        Files.writeString(Paths.get("usageList.txt"), usageListBuilder.toString());

        // Get the average of number of times an image is used
        double averageNumUsages = (double) totalNumUsages / usageCounts.size();


        // Get the results (the largest number of usages and the average number of usages)
        String results = String.format("Largest # of usages: %d %s\n" +
                        "Average # of usages: %f",
                largestNumUsages, filesWithLargestUsages, averageNumUsages);

        Files.writeString(Paths.get("results.txt"), results);

        return results;
    }

}
