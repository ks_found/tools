# ImageUsageFinder

This program finds the average number of times images are used by English Wikipedia.

It does this by finding 1000 random Wikimedia Commons images that are used by English Wikipedia, and counting the number of times Wikipedia uses those images.

The last time the program was run (January 16, 2022), this average was found to be approximately 1.908909.

## Run the program
Java 17+ required.
```
git clone https://gitlab.com/ks_found/imageusagefinder.git
cd imageusagefinder
./gradlew run
```

## Change the sample size
By default, the program processes 1000 images. To change this, change the `IMAGES_TO_PROCESS` variable in `ImageUsageFinder.java`.
